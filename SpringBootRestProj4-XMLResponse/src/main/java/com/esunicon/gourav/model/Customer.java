package com.esunicon.gourav.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Customer {
	private Integer cNo;
	private String cName;
	private String cAdd;
	private Float billAmount;
}
