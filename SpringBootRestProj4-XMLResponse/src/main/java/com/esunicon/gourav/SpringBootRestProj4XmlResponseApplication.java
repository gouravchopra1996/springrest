package com.esunicon.gourav;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRestProj4XmlResponseApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRestProj4XmlResponseApplication.class, args);
	}

}
