package com.esunicon.gourav.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.esunicon.gourav.model.Customer;

@RestController
public class CustomerController {
	/*@GetMapping("/report")
	public Customer showData() {
		Customer cust = new Customer(101, "Gourav", "Hyd", 15000.5f);
		return cust;
	}*/
	@GetMapping("/report")
	public ResponseEntity<Customer> showData() {
		Customer cust = new Customer(101, "Gourav", "Hyd", 15000.5f);
		
		return new ResponseEntity<Customer>(cust, HttpStatus.OK);
	}
}
